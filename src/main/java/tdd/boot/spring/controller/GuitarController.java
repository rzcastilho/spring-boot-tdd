package tdd.boot.spring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tdd.boot.spring.model.Guitar;
import tdd.boot.spring.service.GuitarService;

@RestController
public class GuitarController {

    @Autowired
    private GuitarService guitarService;

    @GetMapping("/guitars/{model}")
    public Guitar getGuitarByModel(@PathVariable String model) {
        return guitarService.getGuitarByModel(model);
    }

}
